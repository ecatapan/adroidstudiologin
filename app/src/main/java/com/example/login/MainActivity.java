package com.example.login;

import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.util.Map;

public class MainActivity extends AppCompatActivity {
    public static final int CONNECTION_TIMEOUT = 10000;
    public static final int READ_TIMEOUT = 15000;
     Button login_btn;
     private EditText myusername;
     private EditText mypassword;
     private JSONObject object;

     //private TextView outputTxt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button login_btn = (Button) findViewById(R.id.login_btn);
        myusername = (EditText) findViewById(R.id.username);
        mypassword = (EditText) findViewById(R.id.password);
        //outputTxt = (TextView) findViewById(R.id.outputTxt);
    }
    public void login_btn(View arg0) {


        final String username = myusername.getText().toString();
        final String password = mypassword.getText().toString();

        // Initialize  AsyncLogin() class with email and password
        new AsyncLogin().execute(username,password);

    }

    public class AsyncLogin extends AsyncTask<String, String, String> {
        ProgressDialog progressLoading = new ProgressDialog(MainActivity.this);
        HttpURLConnection connection = null;
        URL url = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            progressLoading.setMessage("\tLoading...");
            progressLoading.setCancelable(false);
            progressLoading.show();

        }

        @Override
        protected String doInBackground(String... params) {

            try {
                url = new URL("http://192.168.1.5:8080/login/Servlet");
            } catch (MalformedURLException e) {
                e.printStackTrace();
            }
            try {
                connection = (HttpURLConnection) url.openConnection();
                connection.setReadTimeout(READ_TIMEOUT);
                connection.setConnectTimeout(CONNECTION_TIMEOUT);
                connection.setRequestMethod("POST");

                connection.setDoInput(true);
                connection.setDoOutput(true);

                Uri.Builder builder = new Uri.Builder()
                        .appendQueryParameter("username", params[0])
                        .appendQueryParameter("password", params[1]);
                String query = builder.build().getEncodedQuery();

                OutputStream outputStream = connection.getOutputStream();
                BufferedWriter writer = new BufferedWriter(new OutputStreamWriter(outputStream, "UTF-8"));

                writer.write(query);
                writer.flush();
                writer.close();
                outputStream.close();
                connection.connect();
            } catch (IOException e) {
                e.printStackTrace();
              //  return "exception";
            }
            try {
                int response_code = connection.getResponseCode();

                if (response_code == HttpURLConnection.HTTP_OK) {
                    InputStream inputStream = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));


                    String readAPIResponse = " ";
                   StringBuilder object = new StringBuilder();
                   while ((readAPIResponse = reader.readLine()) != null) {
                       object.append(readAPIResponse);
                    }

                   return (object.toString());
                } else {
                   // return ("Unsuccessful");
                }

            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                connection.disconnect();
            }
            return null;
        }

        @Override
        protected void onPostExecute(String object) {

            //this method will be running on UI thread
             super.onPostExecute(object);
             System.out.println(object);
            try {
                JSONObject json = new JSONObject(object);
                //System.out.println(json.toString());
                String result = json.getString("reason");
                if (result.equals("success login")){
                    Intent intent = new Intent(MainActivity.this, SuccessActivity.class);
                    startActivity(intent);
                    MainActivity.this.finish();
                }else {
                    Toast.makeText(getApplicationContext(),"Login Failed!",Toast.LENGTH_SHORT).show();
                }

            } catch (JSONException jsonException) {
                jsonException.printStackTrace();
            }


            //outputTxt.setText(result);
            progressLoading.dismiss();

        }
    }
}

